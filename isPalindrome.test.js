const isPalindrome = require("./isPalindrome");

describe('isPalindrome', () => {

  it('should return false when empty', () => {
    expect(isPalindrome()).toBeFalsy();
  });

  it('should return false when number', () => {
    expect(isPalindrome(12)).toBeFalsy();
  });

  it('should return true when kayak', () => {
    expect(isPalindrome('kayak')).toBeTruthy();
  });

  it('should return true when Kayak', () => {
    expect(isPalindrome('Kayak')).toBeTruthy();
  });

  it('should return false when romane', () => {
    expect(isPalindrome('romane')).toBeFalsy();
  });

  it('should return true when eye', () => {
    expect(isPalindrome('eye')).toBeTruthy();
  });

  it('should return true when eyE', () => {
    expect(isPalindrome('eyE')).toBeTruthy();
  });

  it('should return true when La mariee ira mal', () => {
    expect(isPalindrome('La mariee ira mal')).toBeTruthy();
  });

  it('should return false when queen Roro', () => {
    expect(isPalindrome('queen Roro')).toBeFalsy();
  });
})