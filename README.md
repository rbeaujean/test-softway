# Installation

```
$ git clone https://gitlab.com/rbeaujean/test-softway.git
$ cd test-softway
$ npm install
```

# Technical challenge

## Test 1

In the file isPalindrome.js, could you develop a function that return **true** when string is a palindrome. However, return false.
> A palindrome is a word or group of words that can be read either from left to right or from right to left
> Exemple : 'kayak' | 'eye' | 'la mariee ira mal'

## Test 2

In the file findClosestToZero.js, could you develop a function that resturn the closest number to 0.
> Exemple: [1,2,3] should return 1