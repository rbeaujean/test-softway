const findClosestToZero = require("./findClosestToZero");

describe('findClosestToZero', () => {
  it('should return 0 when [0]', () => {
    expect(findClosestToZero([0])).toBe(0);
  });

  it('should return 1 when [1,2,3]', () => {
    expect(findClosestToZero([1,2,3])).toBe(1);
  });

  it('should return -1 when [-1,2,3]', () => {
    expect(findClosestToZero([-1,2,3])).toBe(-1);
  });

  it('should return -3 when [255,30,30,-4, -3, 57]', () => {
    expect(findClosestToZero([255,30,30,-4, -3, 57])).toBe(-3);
  });

  it('should return null when empty args', () => {
    expect(findClosestToZero()).toBe(null);
  });

  it('should return 1 when empty ["a",10,1,100]', () => {
    expect(findClosestToZero(["a",10,1,100])).toBe(1);
  });
})